import "../Contacts.scss";

import Icon from "../../Icon/Icon";

const Phones = () => {
    return(
        <div className="phones">
           <div className="phones__phone row">
                <Icon src="images/icons/phone.png" alt="phone" height="18px"/>
                <span>
                    <a href="tel:+380965471606">+380965471606</a>
                </span>
           </div>
           <div className="phones__phone row">
                <Icon src="images/icons/phone.png" alt="phone" height="18px"/>
                <span>
                    <a href="tel:+380505621868">+380505621868</a>
                </span>
           </div>
        </div>
    )
}

export default Phones;