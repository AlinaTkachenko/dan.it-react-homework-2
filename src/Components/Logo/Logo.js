import "./Logo.scss";

const Logo = () => {
    return (
        <div className="logo">
            <span className="logo__main">EBK</span>
            <span className="logo__full-name">Electronic Bike<br />Kit's</span>
        </div>
    )
}

export default Logo;