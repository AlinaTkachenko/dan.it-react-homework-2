import PropTypes from 'prop-types';

const ModalImg = ({src, alt}) => {

    return (
        <div className='modal__image'>
            <img src={src} alt={alt}></img>
        </div>
    )
}

ModalImg.defaultProps = {
    alt: "modal-image"
}

ModalImg.propTypes = {
    src: PropTypes.string
  };

export default ModalImg;