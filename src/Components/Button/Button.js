import cn from 'classnames';
import PropTypes from 'prop-types';

import "./Button.scss";

const Button = ({type, optionalClassNames, src, children, onClick}) => {
    return (
        <div className={cn('button-wrapper',optionalClassNames)}>
            <button type={type} onClick={onClick}>
                <img src={src}></img>{children}
            </button>
        </div>
    )
}

Button.defaultProps = {
    type: "button"
  };

Button.propTypes = {
    type: PropTypes.string,
    optionalClassNames: PropTypes.string,
    src: PropTypes.string
  };

export default Button;