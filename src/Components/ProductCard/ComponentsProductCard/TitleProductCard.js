const TitleProductCard = ({children}) => {
    return (
        <div className="product-card__title">
            {children}
        </div>
    )
}

export default TitleProductCard;