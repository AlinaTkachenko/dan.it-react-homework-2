import PropTypes from 'prop-types';

const ImageProductCard = ({src, alt}) => {
    return (
        <div className="product-card__image">
            <img src={src} alt={alt}></img>
        </div>
    )
}

ImageProductCard.defaultProps = {
    alt: "bike"
  };

ImageProductCard.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string
  };

export default ImageProductCard;