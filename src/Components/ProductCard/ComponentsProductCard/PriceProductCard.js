import PropTypes from 'prop-types';

const PriceProductCard = ({price}) => {
    return (
        <div className="product-card__price">
            <span>{price} грн.</span>
        </div>
    )
}

PriceProductCard.propTypes = {
    price: PropTypes.string
  };

export default PriceProductCard;