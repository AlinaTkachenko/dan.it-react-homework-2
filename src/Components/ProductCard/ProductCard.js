import PropTypes from "prop-types";
import cn from "classnames";

import "./ProductCard.scss";

import Button from "../Button/Button";
import ColorProductCard from "./ComponentsProductCard/ColorProductCard";
import ImageProductCard from "./ComponentsProductCard/ImageProductCard";
import PriceProductCard from "./ComponentsProductCard/PriceProductCard";
import TitleProductCard from "./ComponentsProductCard/TitleProductCard";
import {ReactComponent as Heart} from "../Svg/heart.svg";

const ProductCard = ({product, toggleFavorites, favorites, toggleModalCart, handleCurrentPost}) => {
    const{src, title, color, price} = product;

    const toggleActive = (event) => {
        event.target.closest("div").classList.toggle("active");
    }

    const addActiveClass = () => {
        const arrId = favorites.map(item=>item.id);
        if(arrId.includes(product.id)){
            return "active";
        }
    }

    return (
        <div className="product-card">
            <div>
                <ImageProductCard src={src}/>
                <TitleProductCard>{title}</TitleProductCard>
                <ColorProductCard color={color}/>
                <div className={cn("product-card__heart",addActiveClass())} id={product.id} onClick={(event)=> {
                    toggleFavorites(product)
                    toggleActive(event)
                }}>
                    <Heart />
                </div>
            </div>  
            <div className="product-card__wrapper row">
                <PriceProductCard price={price}/>
                <Button src="images/icons/cart-grey.png" 
                    optionalClassNames="product-card__button" 
                    onClick={()=>{
                        toggleModalCart()
                        handleCurrentPost(product)}
                    }>
                </Button>
            </div>          
        </div>
    )
}

ProductCard.propTypes = {
    product: PropTypes.object,
    toggleFavorites: PropTypes.func,
    favorites: PropTypes.array, 
    toggleModalCart: PropTypes.func, 
    handleCurrentPost: PropTypes.func
  };

export default ProductCard;