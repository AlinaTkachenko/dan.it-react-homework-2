import PropTypes from 'prop-types';

import {ReactComponent as Heart} from "../../Svg/heart.svg";

const Favorites = ({favorites}) => {
    return (
        <div className="header__heart">
            <Heart />
            <span>{favorites.length}</span>
        </div>
    )
}

Favorites.propTypes = {
    favorites: PropTypes.array
  };

export default Favorites;