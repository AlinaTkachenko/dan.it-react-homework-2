import NavigationItem from "./NavigationItem";

const Navigation = () => {
    const menuList = ['Головна','Товари','Про нас','Підтримка','Контакти']
    const menuItems = menuList.map((item, index) =>
    <NavigationItem key={index}>{item}</NavigationItem>
  );

    return (
        <nav className="navigation">
            <ul className="navigation__list row">
                {menuItems}
            </ul>
        </nav>
    )
}
export default Navigation;