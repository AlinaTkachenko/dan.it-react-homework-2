import PropTypes from 'prop-types';

import Icon from "../../Icon/Icon";

const IconCart = ({cartProducts}) => {
    return (
        <div className="header__cart-wrapper">
            <Icon src="images/icons/cart.png" alt="cart" width="24px" height="24px"/>
            <span>{cartProducts.length}</span>
        </div>
    )
}

IconCart.propTypes = {
    cartProducts: PropTypes.array
  };

export default IconCart;