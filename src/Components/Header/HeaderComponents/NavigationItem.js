const NavigationItem = ({children}) => {
    return (
        <li className="navigation__item">{children}</li>
    )
}

export default NavigationItem;