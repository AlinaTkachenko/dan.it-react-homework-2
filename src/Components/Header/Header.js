import PropTypes from 'prop-types';

import "./Header.scss";

import Icon from "../Icon/Icon";
import Logo from "../Logo/Logo";
import Navigation from "./HeaderComponents/Navigation";
import Favorites from "./HeaderComponents/Favorites";
import IconCart from "./HeaderComponents/IconCart";

const Header = ({favorites, cartProducts}) => {
    return(
        <div className="header">
            <div className="header__content content row">
                <Logo />
                <div className="header__wrapper row">
                    <Navigation />
                    <div className="header__icons row">
                        <Icon src="images/icons/person.png" alt="person" width="24px" height="24px"/>
                        <IconCart cartProducts={cartProducts}/>
                        <Favorites favorites={favorites}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

Header.propTypes = {
    favorites: PropTypes.array,
    cartProducts: PropTypes.array
  };

export default Header;