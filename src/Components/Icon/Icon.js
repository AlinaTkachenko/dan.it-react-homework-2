import PropTypes from 'prop-types';

import "./Icon.scss";

const Icon = ({src, alt, width, height}) => {
    return (
        <div className="icon" style={{ width: width, height: height}}>
           <img src={src} alt={alt}></img>
        </div>
    )
}

Icon.defaultProps = {
    width: "auto"
}

Icon.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string
  };

export default Icon;