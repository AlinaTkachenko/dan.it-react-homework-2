import {useEffect, useState} from 'react';
import PropTypes from "prop-types";

import "./ProductsWrapper.scss";

import ProductCard from "../ProductCard/ProductCard";
import ModalCart from '../Modal/ModalCart/ModalCart';

const ProductsWrapper = ({favorites, toggleFavorites, addToCart}) => {
    const [products, setProducts] = useState([]);
    const [isOpenModalCart, setIsOpenModalCart] = useState(false);
    const [currentCard, setcurrentCard] = useState({});

    const toggleModalCart = () => {
        setIsOpenModalCart(!isOpenModalCart)
      };

    const handleCurrentPost = (card) => {
        setcurrentCard(card); 
    }

    useEffect(() => {
        fetch("/data.json")
            .then(response => {
                return response.json();    
            })
            .then(data => {
                setProducts(data.bike);
            })
    }, [])

    return (
        <div className="products-wrapper">
            <div className="products">
                {products.map((product)=><ProductCard 
                key={product.id} 
                favorites={favorites} 
                product={product} 
                toggleFavorites={toggleFavorites} 
                toggleModalCart={toggleModalCart}
                handleCurrentPost={handleCurrentPost}></ProductCard>)}
            </div>
            <ModalCart isOpenModalCart={isOpenModalCart} toggleModalCart={toggleModalCart} currentCard={currentCard} addToCart={addToCart}/>
        </div>  
    )
}

ProductsWrapper.propTypes = {
    toggleFavorites: PropTypes.func,
    favorites: PropTypes.array, 
    addToCart: PropTypes.func, 
  };

export default ProductsWrapper;