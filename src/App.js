import React, { useState, useEffect } from 'react';

import './App.scss';

import Footer from './Components/Footer/Footer';
import Header from './Components/Header/Header';
import ProductsWrapper from './Components/ProductsWrapper/ProductsWrapper';

function App() {
  const [favorites,setFavorites] = useState([]);
  const [cartProducts, setCartProducts] = useState([]);

  const toggleFavorites = (product) => {
    
    if(!favorites.find(favorite => favorite.id === product.id)) {
      const newFavorites = [...favorites, product];
      setFavorites(newFavorites);
      localStorage.favorites = JSON.stringify(newFavorites);
    }
    else {
      const newFavorites = favorites.filter(item => product.id != item.id);
      setFavorites(newFavorites);
      localStorage.favorites = JSON.stringify(newFavorites);
    }  
  }

  const addToCart = (product) => {
    const newCartProducts = [...cartProducts,product];
    setCartProducts(newCartProducts);
    localStorage.cardProducts = JSON.stringify(newCartProducts);
  }

  useEffect(() => {  
    if(localStorage.favorites) {
      const startFavorites = JSON.parse(localStorage.favorites);
      setFavorites(startFavorites);
    } 
    if(localStorage.cardProducts) {
      const startCardProducts = JSON.parse(localStorage.cardProducts);
      setCartProducts(startCardProducts);
    } 
  },[]);

  return (
    <div className="App">
      <div className='wrapper'>
        <Header favorites={favorites} cartProducts={cartProducts}/>
        <div className='main content'>
          <ProductsWrapper favorites={favorites} toggleFavorites={toggleFavorites} addToCart={addToCart}></ProductsWrapper>
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default App;
